WebServer server(80);

void handleRoot() {
  String s = MAIN_page; //Read HTML contents
  server.send(200, "text/html", s); //Send web page
}
 
void handleADC() {
  int a = digitalRead(magRight);
  String adcValue = String(a);
  server.send(200, "text/plane", adcValue); //Send ADC value only to client ajax request
}

void handleCleaningState() {
  server.send(200, "text/plane", CleaningState); //Send CleaningState value only to client ajax request 
}

void handleLEDButtonPress() {
  if (digitalRead(LED) == LOW) {
    digitalWrite(LED, HIGH);
  }
  else {
    digitalWrite(LED, LOW);
  }
 server.sendHeader("Location","/"); 
 server.send(303); 
 }
 
void handleDomeLEDButtonPress() {
  if (digitalRead(DomeLED) == LOW) {
    digitalWrite(DomeLED, HIGH);
  }
  else {
    digitalWrite(LED, LOW);
  }
 server.sendHeader("Location","/"); 
 server.send(303); 
 }
 
void handleCleanButtonPress() {
  StartCleaningRequest = true;
  server.sendHeader("Location","/"); 
  server.send(303); 
  }

void startServer() {
  WiFi.mode(WIFI_STA); //Connectto your wifi
  WiFi.begin(ssid, password);
 
  Serial.println("Connecting to ");
  Serial.print(ssid);
 
  while(WiFi.waitForConnectResult() != WL_CONNECTED){      
      Serial.print(".");
    }
  WiFi.setHostname("OpenLitter");
    
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP

  server.on("/", handleRoot);
  server.on("/readADC", handleADC);
  server.on("/readCS", handleCleaningState);
  server.on("/LED", HTTP_POST, handleLEDButtonPress);
<<<<<<< HEAD
  server.on("/DomeLED", HTTP_POST, handleDomeLEDButtonPress);
=======
>>>>>>> cc09f22d9949511a15c321712cccf2379d2c194c
  server.on("/Clean", HTTP_POST, handleCleanButtonPress); 
   
  server.begin();
  Serial.println("HTTP server started"); 
}
