// OpenLitter
// By: Kyle Watson
// email: kyle@cattux.ca
// Oct 18, 2018

// Webserver aspects taken from
// ESP32 AJAX Demo - Updates and Gets data from webpage without page refresh
// https://circuits4you.com
// and
// ESP8266 First Web Server
// https://tttapa.github.io/ESP8266/Chap10%20-%20Simple%20Web%20Server.html

// Declare pins
int IN1 = 16;
int IN2 = 15;
int button = 23;
int DomeLED = 4;
int weight = 34;
int magRight = 36;
int magLeft = 39;
const int LED = 2;

// Declare variables
int buttonState;
int weightState;
unsigned long weightTriggerTime;
int magRightState;
int magLeftState;
bool StartCleaningRequest = false;
String CleaningState = "stopped";
String DomLightState = "off";

// libraries
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
 
#include "index.h"
#include "clean.h"
#include "config.h"
#include "webserver.h"

#include "soc/timer_group_struct.h"
#include "soc/timer_group_reg.h"
 

void setup(void){
  Serial.begin(115200);
  Serial.println();
  Serial.println("Booting Sketch...");

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  pinMode(DomeLED, OUTPUT);
  pinMode(weight, INPUT_PULLUP);
  pinMode(magRight, INPUT);
  pinMode(magLeft, INPUT);
  pinMode(13, OUTPUT);
  
  attachInterrupt(digitalPinToInterrupt(weight), weightChange, CHANGE);
 
  startServer();

  TaskHandle_t Task2;
  xTaskCreatePinnedToCore(
             cpu0Loop,  /* Task function. */
             "Task2",    /* name of task. */
             10000,      /* Stack size of task */
             NULL,       /* parameter of the task */
             1,          /* priority of the task */
             &Task2,     /* Task handle to keep track of created task */
             0);         /* pin task to core 0/1 */
}

void loop(void){
  server.handleClient();

  double weightWaitTime = 420000;
  int weightWaitTimeMin = weightWaitTime/60000;
  
  buttonState = digitalRead(button);
  magLeftState = analogRead(magLeft);
  magRightState = analogRead(magRight);

  if (buttonState == 0){
    Serial.println("Manually Activated!");
    delay(500);  
    StartCleaningRequest = true;
  }
  else if (weightTriggerTime != 0 && millis() > weightTriggerTime + weightWaitTime){
    Serial.print("");
    Serial.print("A cat was here ");
    Serial.print(weightWaitTimeMin);
    Serial.println(" minutes ago.");
    StartCleaningRequest = true;
  }
  TIMERG0.wdt_wprotect=TIMG_WDT_WKEY_VALUE;
  TIMERG0.wdt_feed=1;
  TIMERG0.wdt_wprotect=0;
}

void weightChange(){
  //if weightState == 0, send cat in box
  //if weightState == 1, send cat out of box
  //     start timer before calling Clean_Litter_Box
  digitalWrite(IN1, LOW); 
  digitalWrite(IN2, LOW);
  
  weightState = digitalRead(weight);
  if (weightState == 0){
    weightTriggerTime = millis();
  }
  Serial.println("");
  Serial.print("Weight Sensor is: ");
  Serial.println(weightState);
  Serial.println("");
}

void cpu0Loop(void * pvParameters){
  for(;;){
    if (StartCleaningRequest == true){ 
      cleanLitterBox();
    }
    else{
      delay(1000);
    }
  }
}
