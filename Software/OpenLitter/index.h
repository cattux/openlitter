const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="data:,">
<style>
.card{
    max-width: 400px;
    min-height: 250px;
    background: #02b875;
    padding: 30px;
    box-sizing: border-box;
    color: #FFF;
    margin:20px;
    box-shadow: 0px 2px 18px -4px rgba(0,0,0,0.75);
}
html{
    font-family: Helvetica; 
    display: inline-block; 
    margin: 0px auto; 
    text-align: center;
}
.button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;
    text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;
}
.buttonInactive {background-color: #555555;}
.buttonLEDon {background-color: #4286f4;}
.buttonLEDoff {background-color: #e7bd3b;}
.buttonCancel {background-color: #ff0000;}
</style>
<body> 
  <h1>Open Litter</h1><br>
  <h2>Sensor Value: <span id="ADCValue">0</span></h2>
  <h2>State: <span id="CleaningState">stopped</span></h2>
  <form action="/Clean" method="POST"><button class="button button">Clean</button></form><br>
  <form action="/DomeLED" method="POST"><button class="button buttonLEDon">Dome LED</button></form><br>
  <form action="/LED" method="POST"><button class="button buttonLEDon">LED</button></form>
<script>
 
setInterval(function() {
  // Call a function repetatively with 1 Second interval
  getData();
  getData2();
  sendData();
}, 100); //100mSeconds update rate
 
function getData() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("ADCValue").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "readADC", true);
  xhttp.send();
}
function getData2() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("CleaningState").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "readCS", true);
  xhttp.send();
}
</script>
</body>
</html>
)=====";
