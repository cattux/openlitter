void Wait(int period){
  unsigned long start_time = millis();
  
  while(millis() < start_time + period){
    // wait until given period of time has passed
  }
}

void cleanLitterBox(){
  int dumpPause = 5000;
  int overRotateDelay = 9000;
  int weightDetectedDelay = 10000;

  Serial.println("");
  CleaningState = "Start Cleaning Procedure!";
  Serial.println(CleaningState);
  
  // start spinning globe counter clockwise
  CleaningState = "Spinning globe CCW";
  Serial.println(CleaningState);
  
  digitalWrite(IN1, HIGH); 
  digitalWrite(IN2, LOW);

  // wait for litterbox to be in dump position
  Serial.println("Waiting for magRight == 0");
  while(true){
    //Serial.println(magRight);
    if (analogRead(magRight) == 0){
      digitalWrite(IN1, LOW);
      Serial.println("In dump position");
      break;
    }
    else if (weightState == 1){
      digitalWrite(IN1, LOW);
      Wait(weightDetectedDelay);
      digitalWrite(IN1, HIGH);
    }
    //should add in an option to break out early
  }

  // wait for waste to all fall into bottom tray
  CleaningState = "Wait for all waste to fall";
  Serial.println(CleaningState);
  Wait(dumpPause);

  // start spinning globe clockwise
  CleaningState = "Spinning globe CW";
  Serial.println(CleaningState);
  digitalWrite(IN2, HIGH);
  
  // wait for litterbox to be in home position
  Serial.println("Waiting for magLeft == 0");
  while(true){
    //Serial.println(magLeft);
    if (analogRead(magLeft) == 0){
      Serial.println("In Home position");
      break;
    }
    else if (weightState == 1){
      digitalWrite(IN1, LOW);
      Wait(weightDetectedDelay);
      digitalWrite(IN1, HIGH);
    }
    //should add in an option to break out early
  } 
  // wait X time until over rotation causes litter to fall flat
  CleaningState = "Wait for litter to fall flat";
  Serial.println(CleaningState);
  Wait(overRotateDelay);

  // stop globe
  Serial.println("Stop spinning globe");
  digitalWrite(IN2, LOW);
  
  // start spinning globe counter clockwise
  Serial.println("Spinning globe CCW");
  digitalWrite(IN1, HIGH);

  // wait for litterbox to be in home position
  Serial.println("Waiting for magLeft == 0");
  while(true){
    //Serial.println(magLeft);
    if (analogRead(magLeft) == 0){
      Serial.println("In Home position");
      CleaningState = "In Home position";
      digitalWrite(IN1, LOW);
      Serial.println("Stop globe");
      break;
    }
    else if (weightState == 1){
      digitalWrite(IN1, LOW);
      Wait(weightDetectedDelay);
      digitalWrite(IN1, HIGH);
    }
    //should add in an option to break out early
  }
  Serial.println("Finished Cleaning Procedure!");
  weightTriggerTime = 0;
  Serial.println("");
  StartCleaningRequest = false;
  CleaningState = "stopped";
  Serial.println(CleaningState);
}
