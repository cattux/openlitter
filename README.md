# OpenLitter

An open source replacement controller for the Litter Robot 3. Rather than spend 80 USD for a replacement controller, this aims to be an open source replacement so that features can be added. 

Added features such as WiFi and MQTT. The main drive behind this project was to be able to push user data to an instance of Home Assistant. Ideally to be able to receive notifications that the litter box is full, or show usage statics if you're concerned about your cats bathroom schedule. The skys the limit, but this is really only the means of getting the data exposed.

The scope of the project is to include both the hardware and software required to retrofit a Litter Robot 3 with a new controller. The plan is to design the hardware to be a complete drop-in replacement. Once installed, the LR3 will ideally behave identically to a stock unit unless programmed to do otherwise. The hardware would include the PCB design as well as the bill of material (BOM) required to populate the PCB. The software will be what reads the sensors, controls the motor, and publishes the data.