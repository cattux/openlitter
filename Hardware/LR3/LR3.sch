EESchema Schematic File Version 4
LIBS:LR3-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5BBAC714
P 2950 1950
F 0 "J1" H 3000 2267 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 3000 2176 50  0000 C CNN
F 2 "DF11:hrs-df11c-6dp-2v" H 2950 1950 50  0001 C CNN
F 3 "~" H 2950 1950 50  0001 C CNN
	1    2950 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J2
U 1 1 5BBAC7F6
P 2950 2750
F 0 "J2" H 3000 3067 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 3000 2976 50  0000 C CNN
F 2 "DF11:hrs-df11c-8dp-2v" H 2950 2750 50  0001 C CNN
F 3 "~" H 2950 2750 50  0001 C CNN
	1    2950 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J4
U 1 1 5BBAC8AA
P 2950 3500
F 0 "J4" H 3000 3717 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3000 3626 50  0000 C CNN
F 2 "DF11:hrs-df11c-4dp-2v" H 2950 3500 50  0001 C CNN
F 3 "~" H 2950 3500 50  0001 C CNN
	1    2950 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 5BBAC972
P 3000 1050
F 0 "J3" H 3050 1267 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3050 1176 50  0000 C CNN
F 2 "DF11:hrs-df11c-4dp-2v" H 3000 1050 50  0001 C CNN
F 3 "~" H 3000 1050 50  0001 C CNN
	1    3000 1050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5BBACA7D
P 900 900
F 0 "#FLG?" H 900 975 50  0001 C CNN
F 1 "PWR_FLAG" H 900 1074 50  0000 C CNN
F 2 "" H 900 900 50  0001 C CNN
F 3 "~" H 900 900 50  0001 C CNN
	1    900  900 
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5BBACAA7
P 1300 900
F 0 "#FLG?" H 1300 975 50  0001 C CNN
F 1 "PWR_FLAG" H 1300 1074 50  0000 C CNN
F 2 "" H 1300 900 50  0001 C CNN
F 3 "~" H 1300 900 50  0001 C CNN
	1    1300 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+15V #PWR?
U 1 1 5BBACADF
P 900 1350
F 0 "#PWR?" H 900 1200 50  0001 C CNN
F 1 "+15V" H 915 1523 50  0000 C CNN
F 2 "" H 900 1350 50  0001 C CNN
F 3 "" H 900 1350 50  0001 C CNN
	1    900  1350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BBACB2E
P 1300 1350
F 0 "#PWR?" H 1300 1100 50  0001 C CNN
F 1 "GND" H 1305 1177 50  0000 C CNN
F 2 "" H 1300 1350 50  0001 C CNN
F 3 "" H 1300 1350 50  0001 C CNN
	1    1300 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  900  900  1350
Wire Wire Line
	1300 1350 1300 900 
$Comp
L Connector_Generic:Conn_02x02_Odd_Even P3
U 1 1 5BBA5253
P 7450 800
F 0 "P3" H 7500 1017 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 7500 926 50  0000 C CNN
F 2 "DF11:hrs-df11c-4dp-2v" H 7450 800 50  0001 C CNN
F 3 "~" H 7450 800 50  0001 C CNN
	1    7450 800 
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even P1
U 1 1 5BBA52A1
P 7450 1250
F 0 "P1" H 7500 1567 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 7500 1476 50  0000 C CNN
F 2 "DF11:hrs-df11c-6dp-2v" H 7450 1250 50  0001 C CNN
F 3 "~" H 7450 1250 50  0001 C CNN
	1    7450 1250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J?
U 1 1 5BBA52E9
P 2950 2750
F 0 "J?" H 3000 3067 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 3000 2976 50  0000 C CNN
F 2 "DF11:hrs-df11c-8dp-2v" H 2950 2750 50  0001 C CNN
F 3 "~" H 2950 2750 50  0001 C CNN
	1    2950 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even P2
U 1 1 5BBA5394
P 7950 2900
F 0 "P2" H 8000 3217 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 8000 3126 50  0000 C CNN
F 2 "DF11:hrs-df11c-8dp-2v" H 7950 2900 50  0001 C CNN
F 3 "~" H 7950 2900 50  0001 C CNN
	1    7950 2900
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even P4
U 1 1 5BBA5441
P 7950 4100
F 0 "P4" H 8000 4317 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 8000 4226 50  0000 C CNN
F 2 "DF11:hrs-df11c-4dp-2v" H 7950 4100 50  0001 C CNN
F 3 "~" H 7950 4100 50  0001 C CNN
	1    7950 4100
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5BBA593D
P 8100 1950
F 0 "J?" H 8206 2128 50  0000 C CNN
F 1 "LED CCA" H 8206 2037 50  0000 C CNN
F 2 "" H 8100 1950 50  0001 C CNN
F 3 "~" H 8100 1950 50  0001 C CNN
	1    8100 1950
	1    0    0    -1  
$EndComp
Text Label 8850 2050 2    50   ~ 0
Purple
Text Label 8850 1950 2    50   ~ 0
Black
$Comp
L Connector:Conn_01x03_Male U?
U 1 1 5BBA5BE6
P 10900 2900
F 0 "U?" H 11006 3178 50  0000 C CNN
F 1 "LeftMagSensor" H 11006 3087 50  0000 C CNN
F 2 "" H 10900 2900 50  0001 C CNN
F 3 "~" H 10900 2900 50  0001 C CNN
	1    10900 2900
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male U?
U 1 1 5BBA5C46
P 10850 2250
F 0 "U?" H 10956 2528 50  0000 C CNN
F 1 "RightMagSensor" H 10956 2437 50  0000 C CNN
F 2 "" H 10850 2250 50  0001 C CNN
F 3 "~" H 10850 2250 50  0001 C CNN
	1    10850 2250
	-1   0    0    1   
$EndComp
Text Label 10700 3000 2    50   ~ 0
DarkGreen
Text Label 10700 2900 2    50   ~ 0
Black
Text Label 10700 2800 2    50   ~ 0
Orange
Text Label 10600 2350 2    50   ~ 0
Yellow
Text Label 10600 2250 2    50   ~ 0
Black
Text Label 10600 2150 2    50   ~ 0
DarkGreen
Wire Wire Line
	7650 1350 8300 1350
Text Label 7750 1350 0    50   ~ 0
White
Text Label 7750 1250 0    50   ~ 0
DarkGreen
Text Label 7750 1150 0    50   ~ 0
Brown
Wire Wire Line
	7150 1350 6500 1350
Text Label 6700 1150 0    50   ~ 0
Red
Text Label 6700 1250 0    50   ~ 0
Black
Text Label 6700 1350 0    50   ~ 0
Black
Wire Wire Line
	7650 4100 7000 4100
Text Label 8250 4000 0    50   ~ 0
Red
Text Label 8250 4100 0    50   ~ 0
Blue
Text Label 7150 4000 0    50   ~ 0
Yellow
Text Label 7150 4100 0    50   ~ 0
Black
Wire Wire Line
	7650 3000 7000 3000
Wire Wire Line
	7650 2700 7000 2700
Text Label 8250 3000 0    50   ~ 0
DarkGreen
Text Label 8250 2900 0    50   ~ 0
Orange
Text Label 8250 2800 0    50   ~ 0
Blue
Text Label 8250 2700 0    50   ~ 0
Purple
Text Label 7200 3000 0    50   ~ 0
Yellow
Text Label 7200 2900 0    50   ~ 0
Black
Text Label 7200 2800 0    50   ~ 0
Black
Text Label 7200 2700 0    50   ~ 0
Black
Text Label 8550 1150 0    50   ~ 0
White
Text Label 8550 1250 0    50   ~ 0
Red
Text Label 9350 1250 2    50   ~ 0
Motor+
Text Label 9350 1150 2    50   ~ 0
Motor-
Wire Wire Line
	7650 1150 9350 1150
Wire Wire Line
	7650 1250 9350 1250
Wire Wire Line
	7000 3350 7000 3000
Wire Wire Line
	10200 2150 10200 3000
Wire Wire Line
	10200 3000 10700 3000
Wire Wire Line
	10200 2150 10650 2150
Connection ~ 10200 3000
Wire Wire Line
	10300 2250 10300 2900
Wire Wire Line
	10300 2900 10700 2900
Wire Wire Line
	10300 2250 10650 2250
Wire Wire Line
	9600 2350 10650 2350
Wire Wire Line
	8150 3000 10200 3000
Wire Wire Line
	9450 2900 9450 2800
Wire Wire Line
	9450 2800 10700 2800
Wire Wire Line
	8150 2900 9450 2900
Wire Wire Line
	9600 2350 9600 3350
Wire Wire Line
	7000 3350 9600 3350
Wire Wire Line
	10300 2900 9700 2900
Wire Wire Line
	9700 2900 9700 3450
Wire Wire Line
	9700 3450 6900 3450
Wire Wire Line
	6900 3450 6900 2900
Connection ~ 10300 2900
Wire Wire Line
	6900 2900 7650 2900
$Comp
L power:GND #PWR?
U 1 1 5BBCDBA8
P 6500 1350
F 0 "#PWR?" H 6500 1100 50  0001 C CNN
F 1 "GND" H 6505 1177 50  0000 C CNN
F 2 "" H 6500 1350 50  0001 C CNN
F 3 "" H 6500 1350 50  0001 C CNN
	1    6500 1350
	1    0    0    -1  
$EndComp
$Comp
L power:+15V #PWR?
U 1 1 5BBCDC0F
P 8300 1350
F 0 "#PWR?" H 8300 1200 50  0001 C CNN
F 1 "+15V" H 8315 1523 50  0000 C CNN
F 2 "" H 8300 1350 50  0001 C CNN
F 3 "" H 8300 1350 50  0001 C CNN
	1    8300 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	6100 1250 7150 1250
Wire Wire Line
	7150 1150 6100 1150
Text Label 6450 1150 2    50   ~ 0
Battery+
Text Label 6450 1250 2    50   ~ 0
Battery-
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5BBD3903
P 6950 1900
F 0 "J?" H 7056 2078 50  0000 C CNN
F 1 "WeightSensor" H 7056 1987 50  0000 C CNN
F 2 "" H 6950 1900 50  0001 C CNN
F 3 "~" H 6950 1900 50  0001 C CNN
	1    6950 1900
	1    0    0    -1  
$EndComp
Text Label 7650 2000 2    50   ~ 0
Blue
Text Label 7650 1900 2    50   ~ 0
Black
Wire Wire Line
	9150 2050 9150 2700
Wire Wire Line
	8150 2700 9150 2700
Wire Wire Line
	9050 1950 9050 2300
Wire Wire Line
	9050 2300 7000 2300
Wire Wire Line
	7000 2300 7000 2700
Wire Wire Line
	8300 1950 9050 1950
Wire Wire Line
	7800 1900 7800 2200
Wire Wire Line
	7800 2200 6900 2200
Wire Wire Line
	6900 2200 6900 2800
Wire Wire Line
	6900 2800 7650 2800
Wire Wire Line
	7900 2000 7900 2200
Wire Wire Line
	7900 2200 8950 2200
Wire Wire Line
	8950 2200 8950 2800
Wire Wire Line
	8150 2800 8950 2800
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5BBC08A8
P 7600 5350
F 0 "J?" H 7700 5550 50  0000 C CNN
F 1 "Conn_01x04_Male" H 7700 5650 50  0000 C CNN
F 2 "" H 7600 5350 50  0001 C CNN
F 3 "~" H 7600 5350 50  0001 C CNN
	1    7600 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 2050 9150 2050
Wire Wire Line
	7150 1900 7800 1900
Wire Wire Line
	7150 2000 7900 2000
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5BBC6A7C
P 7100 5450
F 0 "J?" H 7250 5150 50  0000 R CNN
F 1 "Right DFI" H 7350 5050 50  0000 R CNN
F 2 "" H 7100 5450 50  0001 C CNN
F 3 "~" H 7100 5450 50  0001 C CNN
	1    7100 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	7800 5250 9000 5250
Wire Wire Line
	9000 5250 9000 4100
Wire Wire Line
	8150 4100 9000 4100
Wire Wire Line
	7000 4100 7000 4300
Wire Wire Line
	7000 4300 8900 4300
Wire Wire Line
	8900 4300 8900 5350
Wire Wire Line
	8900 5350 7800 5350
Wire Wire Line
	9100 4000 9100 5450
Wire Wire Line
	9100 5450 7800 5450
Wire Wire Line
	8150 4000 9100 4000
Wire Wire Line
	6900 4000 6900 4400
Wire Wire Line
	6900 4400 8800 4400
Wire Wire Line
	8800 4400 8800 5550
Wire Wire Line
	8800 5550 7800 5550
Wire Wire Line
	6900 4000 7650 4000
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5BBCAF36
P 5700 5350
F 0 "J?" H 5850 5550 50  0000 R CNN
F 1 "Left DFI" H 6000 5650 50  0000 R CNN
F 2 "" H 5700 5350 50  0001 C CNN
F 3 "~" H 5700 5350 50  0001 C CNN
	1    5700 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5250 6400 5250
Wire Wire Line
	6400 5250 6400 5550
Wire Wire Line
	6400 5550 6900 5550
Wire Wire Line
	5900 5350 6500 5350
Wire Wire Line
	6500 5350 6500 5450
Wire Wire Line
	6500 5450 6900 5450
Wire Wire Line
	6900 5350 6600 5350
Wire Wire Line
	6600 5350 6600 5400
Wire Wire Line
	6600 5400 6300 5400
Wire Wire Line
	6300 5400 6300 5450
Wire Wire Line
	6300 5450 5900 5450
Wire Wire Line
	5900 5550 6350 5550
Wire Wire Line
	6350 5550 6350 5500
Wire Wire Line
	6350 5500 6450 5500
Wire Wire Line
	6450 5500 6450 5250
Wire Wire Line
	6450 5250 6900 5250
$Comp
L Switch:SW_Push SW?
U 1 1 5BBD3FE7
P 4700 5300
F 0 "SW?" H 4700 5585 50  0000 C CNN
F 1 "Pinch Switch" H 4700 5494 50  0000 C CNN
F 2 "" H 4700 5500 50  0001 C CNN
F 3 "" H 4700 5500 50  0001 C CNN
	1    4700 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5BBD4086
P 5400 5400
F 0 "J?" H 5500 5150 50  0000 R CNN
F 1 "Pinch connector" H 5750 5500 50  0000 R CNN
F 2 "" H 5400 5400 50  0001 C CNN
F 3 "~" H 5400 5400 50  0001 C CNN
	1    5400 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5200 5300 4900 5300
Wire Wire Line
	4500 5300 4500 5400
Wire Wire Line
	4500 5400 5200 5400
Text Notes 4600 5900 0    50   ~ 0
Not sure what gets transfered over these lines, or how it knows when there is a pinch. Will need to do some trial and error it seems.\nNeed to recreate these CCAs are a custom component.\nI suspect it's just power and ground and then a wire for \ntray full and a wire for pinch sensor.
Text Notes 5350 5000 0    50   ~ 0
Left DFI CCA
Text Notes 7100 5000 0    50   ~ 0
Right DFI CCA
Text Notes 9750 3450 0    50   ~ 0
S01E\ncan't find spec sheets yet. \nLast 4 digits appear to be date code.
$EndSCHEMATC
